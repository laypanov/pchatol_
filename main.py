#! /usr/bin/env python
# -*- coding: utf-8 -*-

import pymssql
import wsgiserver
import urlparse
from dto9base import *
from dto9fptr import *
from datetime import datetime, timedelta
import os

reload(sys)
sys.setdefaultencoding("utf-8")


class KktDrv(object):
    def __init__(self):
        self.kkt_inf = ""
        self.error_kkt = ""
        self.sql = SqlData()

    def __call__(self, func, **kwargs):
        locals()[func]('d', **kwargs)

    def check_error(self, f):
        try:
            erc = f.get_ResultCode()
            if erc < 0:
                erd = f.get_ResultDescription()
                self.error_kkt = "".join("%s, %s;" % (erc, erd))
                return False
            return True
        except:
            return False

    # port = USB, TCPIP
    # model = 'ModelATOL30F': 61, 'ModelATOL25F': 57
    # Подключение к ККТ
    # connect(0, 'USB', 61, 0x2912, 0x0005, '0.0.0.0', 0)
    def connect(self, protocol=0, port='USB', model=0, vid=0x0000, pid=0x0000, ip_address='0.0.0.0', ip_port=0,
                **kwargs):
        if os.uname()[4] == "x86_64":
            bit = 64
        else:
            bit = 32
        lib = os.path.abspath(os.curdir + '/drv/linux_%s/libfptr.so' % bit)
        lib_dir = os.path.abspath(os.curdir + '/drv/linux_%s/' % bit)
        f = Fptr(lib, -1)  # -1. иначе не работает!

        f.put_DeviceSingleSetting('Port', port)
        f.put_DeviceSingleSetting('SearchDir', lib_dir)
        f.put_DeviceSingleSetting('Protocol', protocol)
        f.put_DeviceSingleSetting('Model54', model)
        f.put_DeviceSingleSetting('Model', model)
        f.put_DeviceSingleSetting('AccessPassword', 0)
        f.put_DeviceSingleSetting('UserPassword', 30)
        f.put_DeviceSingleSetting('OfdPort', 'NONE')
        if ip_address and ip_port:
            f.put_DeviceSingleSetting('IPAddress', ip_address)
            f.put_DeviceSingleSetting('IPPort', ip_port)
        else:
            f.put_DeviceSingleSetting('Vid', vid)
            f.put_DeviceSingleSetting('Pid', pid)

        f.ApplySingleSettings()
        f.put_DeviceEnabled(1)
        if not self.check_error(f): return
        return f

    # X Отчет
    def report_x(self, f, **kwargs):
        f.put_Mode(2)
        f.SetMode()
        if not self.check_error(f): return
        f.put_ReportType(2)
        f.Report()
        if not self.check_error(f): return
        f.ResetMode()
        if not self.check_error(f): return
        return True

    # Открытие смены
    def open_session(self, f, session_casher, **kwargs):
        f.put_Mode(1)
        if not self.check_error(f): return
        f.SetMode()
        if not self.check_error(f): return
        f.put_FiscalPropertyNumber(1021)
        f.put_FiscalPropertyPrint(1)
        f.put_FiscalPropertyType(5)
        f.put_FiscalPropertyValue('%s' % session_casher)
        f.WriteFiscalProperty()
        f.OpenSession()
        if not self.check_error(f): return
        self.session_number = f.get_Session()
        if not self.check_error(f): return
        return True

    # Закрытие смены
    def close_session(self, f, **kwargs):
        f.put_Mode(3)
        if not self.check_error(f): return
        f.SetMode()
        if not self.check_error(f): return
        f.put_ReportType(1)
        f.Report()
        if not self.check_error(f): return
        self.session_number = f.get_Session()
        if not self.check_error(f): return
        return True

    # Закрыть открытый чек
    def cancel_receipt(self, f, **kwargs):
        f.put_Mode(1)
        f.CancelCheck()
        return True

    # Проверка доступности ККТ
    def cancel_receipt(self, f, **kwargs):
        f.put_Mode(1)
        f.CancelCheck()
        return True

    def create_receipt_sell(self, f, recid, session_casher, def_psn, phone, email, def_nds, payment_type, type_close,
                            check_type, goods, **kwargs):
        # Имя кассира, Применяемая система налогооблажения, НДС, отправка смс или на почту чека

        # Чек (put_CheckType)
        # ChequeClosed                  Чек закрыт                                  0
        # ChequeSell                    Чек продажи / прихода                       1
        # ChequeSellReturn              Чек возврат продажи / прихода               2
        # ChequeSellAnnulate            Чек аннулирования продажи                   3
        # ChequeSellCorrection          Чек коррекции прихода                       7
        # ChequeSellReturnCorrection    Чек коррекции возврата прихода              8

        # ChequeBuy                     Чек покупки / расхода                       4
        # ChequeBuyReturn               Чек возврата покупки / расхода              5
        # ChequeBuyAnnulate             Чек аннулирования покупки                   6
        # ChequeBuyCorrection           Чек коррекции    расхода                    9
        # ChequeBuyReturnCorrection     Чек коррекции возврата расхода              10

        # Применяемая система налогооблажения в чеке:
        # ОСН - 1
        # УСН доход - 2
        # УСН доход-расход - 4
        # ЕНВД - 8
        # ЕСН - 16
        # ПСН - 32

        # TaxTypeNumber - Номер налога:
        # 0 - Налог из секции
        # 1 - НДС 0%
        # 2 - НДС 10%
        # 3 - НДС 18% (18%)
        # 4 - НДС не облагается
        # 5 - НДС с расчётной ставкой 10% (110/10)
        # 6 - НДС с расчётной ставкой 18% (118/18)

        # put_PositionPaymentType
        # 1 - Приход
        # 2 - Предоплата
        # 3 - Аванс

        # Оплата и закрытие чека
        # TypeClose - Тип оплаты:
        # 0 - Наличными
        # 1 - Электронными средствами платежа
        sum_all = 0
        positionsum = 0

        # Печать любой, не фискальной информации в чеке
        f.put_Caption('################################')
        f.PrintString()
        f.put_Caption('recid: %s' % recid)
        f.PrintString()
        f.put_Caption('################################')
        f.PrintString()
        f.put_Caption('')
        f.PrintString()

        f.put_Mode(1)
        f.SetMode()
        if not self.check_error(f): return

        f.put_CheckType(check_type)

        # 1 - Приход
        f.put_PositionPaymentType(payment_type)

        f.put_TestMode(0)
        f.put_PrintCheck(1)
        f.OpenCheck()
        if not self.check_error(f): return

        # Записать имя кассира
        f.put_FiscalPropertyNumber(1021)
        f.put_FiscalPropertyPrint(1)
        f.put_FiscalPropertyType(5)
        f.put_FiscalPropertyValue(u'%s' % session_casher)
        f.WriteFiscalProperty()
        if not self.check_error(f): return

        # Применяемая система налогооблажения
        f.put_FiscalPropertyNumber(1055)
        f.put_FiscalPropertyValue(def_psn)
        f.put_FiscalPropertyType(1)
        f.WriteFiscalProperty()
        if not self.check_error(f): return

        # электронный адрес или телефон покупателя
        if email or phone:
            f.put_FiscalPropertyNumber(1008)
            f.put_FiscalPropertyPrint(1)
            f.put_FiscalPropertyType(5)
            f.put_FiscalPropertyValue('%s%s' % (email, phone))
            f.WriteFiscalProperty()
            if not self.check_error(f): return

        # печать товара
        for g in goods:
            # сумма на позицию = цена * кол-во
            positionsum = g[4] * g[5]
            sum_all += positionsum

            f.put_Name(g[2])
            f.put_Price(g[5])
            f.put_Quantity(g[4])
            f.put_Department(0)
            f.put_PositionSum(positionsum)
            f.put_Summ(0.000000)  # скидка
            f.PrintString()
            f.put_TaxNumber(def_nds)
            f.Registration()
            if not self.check_error(f): return

        f.put_TypeClose(type_close)
        f.put_Summ(sum_all)
        f.Payment()
        if not self.check_error(f): return
        f.CloseCheck()
        if not self.check_error(f): return
        return True

    def create_receipt_sell_return(self, f, recid, session_casher, def_psn, phone, email, def_nds, payment_type,
                                   type_close,
                                   check_type, goods, **kwargs):
        sum_all = 0
        positionsum = 0

        # Печать любой, не фискальной информации в чеке
        f.put_Caption('################################')
        f.PrintString()
        f.put_Caption('recid: %s' % recid)
        f.PrintString()
        f.put_Caption('################################')
        f.PrintString()
        f.put_Caption('')
        f.PrintString()

        f.put_Mode(1)
        f.SetMode()
        if not self.check_error(f): return

        f.put_CheckType(2)

        # 1 - Приход
        f.put_PositionPaymentType(payment_type)

        f.put_TestMode(0)
        f.put_PrintCheck(1)
        f.OpenCheck()
        if not self.check_error(f): return

        # Записать имя кассира
        f.put_FiscalPropertyNumber(1021)
        f.put_FiscalPropertyPrint(1)
        f.put_FiscalPropertyType(5)
        f.put_FiscalPropertyValue(u'%s' % session_casher)
        f.WriteFiscalProperty()
        if not self.check_error(f): return

        # Применяемая система налогооблажения
        f.put_FiscalPropertyNumber(1055)
        f.put_FiscalPropertyValue(def_psn)
        f.put_FiscalPropertyType(1)
        f.WriteFiscalProperty()
        if not self.check_error(f): return

        # электронный адрес или телефон покупателя
        if email or phone:
            f.put_FiscalPropertyNumber(1008)
            f.put_FiscalPropertyPrint(1)
            f.put_FiscalPropertyType(5)
            f.put_FiscalPropertyValue('%s%s' % (email, phone))
            f.WriteFiscalProperty()
            if not self.check_error(f): return

        # печать товара
        for g in goods:
            # сумма на позицию = цена * кол-во
            positionsum = g[4] * g[5]
            sum_all += positionsum

            f.put_Name(g[2])
            f.put_Price(g[5])
            f.put_Quantity(g[4])
            f.put_Department(0)
            f.put_PositionSum(positionsum)
            f.put_Summ(0.000000)  # скидка
            f.PrintString()
            f.put_TaxNumber(def_nds)
            f.Registration()
            if not self.check_error(f): return

        f.put_TypeClose(type_close)
        f.put_Summ(sum_all)
        f.Payment()
        if not self.check_error(f): return
        f.CloseCheck()
        if not self.check_error(f): return
        return True

class SqlData(object):
    def __init__(self):
        self.error_sql = ""
        self.conn = ""

    def sql_exec(self, sql, d):
        try:
            cursor = self.sql_connect()
            if d == "fetchone":
                cursor.execute(sql)
                data = cursor.fetchone()
            elif d == "fetchall":
                cursor.execute(sql)
                data = cursor.fetchall()
            elif d == "update":
                cursor.execute(sql)
                self.conn.commit()
                data = ""
            elif d == "insert":
                cursor.execute(sql)
                cursor.execute("select scope_identity()")
                data = cursor.fetchone()
                self.conn.commit()
            cursor.close()
            self.conn.close()
            return data
        except Exception as e:
            self.error_sql = "".join("%s" % e)

    # получение данных о ккм
    def select_cm_sets(self, kkt_id):
        query = self.sql_exec("select def_psn, def_nds, protocol, port, model, vid, pid, ip_address, ip_port, firm_id "
                              "from [0R_CM_sets] where id = %s" % kkt_id, 'fetchone')
        if query:
            return query
        else:
            self.error_sql = "".join("Нет ККТ с таким ИД")

    def select_cm_session(self, kkt_id):
        query = self.sql_exec("select [0R_CM_session].casher \
                                from [0r_cm_sets], [0R_CM_session] \
                                where [0r_cm_sets].session_id = [0R_CM_session].id and [0r_cm_sets].id =  %s" \
                              % kkt_id, 'fetchone')
        if query[0]:
            return query[0]
        else:
            self.error_sql = "".join("Сессия в БД у ККТ не октрыта")

    # инфомрация о заголовках
    def select_head(self, recid):
        query = self.sql_exec("""select  email, 
                                         phone, 
                                         status, 
                                         user_name,
                                         send_type,
                                         payment_type, 
                                         type_close, 
                                         check_type,
                                         firm_id
                                    from [0R_CM_docs]
                                    where recid = %s""" % recid, "fetchone")
        if query:
            return query
        else:
            self.error_sql = "".join("Ошибка получения данных из БД. (select_head)")

    # получение информации о товаре
    def select_body(self, recid):
        query = self.sql_exec("""select  oper.kodtov, 
                                             artikul=rtrim(K_KodTov.artikul), 
                                             [name]=rtrim(K_KodTov.tov_name), 
                                             edizm=rtrim(K_KodTov.tov_edizm), 
                                             oper.kolvo, oper.cena 
                                        from [0R_CM_oper] oper, [Gep_MsSQL].[dbo].K_KodTov K_KodTov
                                        where oper.kodtov = K_KodTov.TOV_KOD and RecID = %s
                                        order by K_KodTov.artikul""" % recid, "fetchall")
        if query:
            return query
        else:
            self.error_sql = "".join("Ошибка получения данных из БД. (select_body)")

    # обновление статуса чека и запись фискальной информации
    def update_status_receipt(self, recid, status, fd="", fpd=""):
        self.sql_exec("""update [0R_CM_docs] 
                        set status = %s, fd = %s, fpd = %s
                        where RecID = %s""" % (status, fd, fpd, recid), "update")

    # запись логов
    def insert_log(self, session_casher, kkt_id, recid, user_name, check_type, errors="", task_result=0):
        d = {0: "Ошибка записи лога в БД"}
        query = self.sql_exec("insert into  dbo.[0R_CM_logs](session_casher, kkt_id, task_title, recid, "
                              "task_result, user_name, task_info) \
                               values('%s', '%s', '%s', '%s', '%s', '%s', '%s');"
                              % (session_casher, kkt_id, check_type, recid, task_result, user_name, "".join(errors)),
                              "insert")
        if query:
            return query
        else:
            return d


class HttpOutput(object):
    def __init__(self):
        self.kkt_id = None
        self.kkt_inf = None
        self.sql = SqlData()
        self.kkt = KktDrv()
        self.error_other = ""
        server = wsgiserver.WSGIServer(self.http_kkt, host='0.0.0.0', port=8080)
        server.start()

    def check_all_errors(self, session_casher='', kkt_id='', recid='', check_type='', user_name='', error_other='',
                         task_result=0):
        errors = "".join("%s %s" % (self.kkt.error_kkt.encode('utf-8'), error_other))

        # очистка переменных с ошибками
        self.sql.error_sql = ""
        self.kkt.error_kkt = ""

        if self.sql.error_sql or self.kkt.error_kkt or error_other:
            query = self.sql.insert_log(session_casher, kkt_id, recid, user_name, check_type, errors, task_result)
        elif task_result == 1:
            query = self.sql.insert_log(session_casher, kkt_id, recid, user_name, check_type, task_result=1)
            return "1; %s; %s" % (query[0], errors)
        elif error_other:
            query = self.sql.insert_log(session_casher, kkt_id, recid, user_name, check_type, error_other, task_result)
        else:
            errors = "500, %s данные получены неправильно" % errors
            query = self.sql.insert_log(session_casher, kkt_id, recid, user_name, check_type, errors, task_result)

        return "0; %s; %s" % (query[0], errors)

    def http_kkt(self, environ, start_response):
        cr = None
        check_type = ""
        session_casher = ""

        start_response('200 OK', [
            ('Content-Type', 'text/html; charset=utf-8')
        ])

        qs = urlparse.parse_qs(environ['QUERY_STRING'])
        env_p = environ['PATH_INFO'][1:-1]
        if env_p != "" and env_p != "favicon.ic":
            kkt_id = qs.get('kkt_id', [''])[0]
            recid = qs.get('recid', [''])[0]

            if env_p == "services":
                check_type = int(qs.get('check_type', [''])[0])
                session_casher = qs.get('session_casher', [''])[0]
            user_name = qs.get('user_name', [''])[0]

            if kkt_id != "":
                kkt_inf = self.sql.select_cm_sets(kkt_id)
                if kkt_inf:
                    kwargs = {
                        "recid": recid,
                        "def_psn": kkt_inf[0],
                        "def_nds": kkt_inf[1],
                        "protocol": kkt_inf[2],
                        "port": kkt_inf[3],
                        "model": kkt_inf[4],
                        "vid": kkt_inf[5],
                        "pid": kkt_inf[6],
                        "ip_address": kkt_inf[7],
                        "ip_port": kkt_inf[8],
                        "firm_id": kkt_inf[9],
                    }
                    if env_p == "services":
                        kwargs = dict(kwargs, session_casher=session_casher)
                    else:
                        kwargs = dict(kwargs, session_casher=self.sql.select_cm_session(kkt_id))

                    f = self.kkt.connect(**kwargs)
                    if f:
                        # добавляем к переменным, драйвер ккт
                        kwargs = dict(kwargs, f=f)

                        # предварительная проврека перед созданием чека
                        if env_p == "create_receipt":
                            query_sh = self.sql.select_head(recid)
                            query_sb = self.sql.select_body(recid)
                            if query_sh and query_sb:
                                email = query_sh[0]
                                phone = query_sh[1]
                                status = query_sh[2]
                                send_type = query_sh[4]
                                payment_type = query_sh[5]
                                type_close = query_sh[6]
                                check_type = query_sh[7]
                                firm_id = query_sh[8]

                                goods = query_sb
                                # проверка статуса чека
                                if status == 1:
                                    self.sql.update_status_receipt(recid, "2")
                                    kwargs2 = {
                                        "send_type": send_type,
                                        "email": email,
                                        "phone": phone,
                                        "check_type": check_type,
                                        "payment_type": payment_type,
                                        "type_close": type_close,
                                        "goods": goods
                                    }
                                    kwargs = dict(list(kwargs.items()) + list(kwargs2.items()))

                                    # проверка фирмы в чеке и в ккт
                                    if kwargs['firm_id'] != firm_id:
                                        error_other = 'Попытка печатать чек на ККМ другого юр. лица'
                                        self.sql.update_status_receipt(recid, "-1")
                                        return self.check_all_errors(kwargs['session_casher'], kkt_id, recid,
                                                                     check_type, user_name, error_other)
                                    if not kwargs['session_casher']:
                                        error_other = 'Попытка печатати чека при закрытой смене'
                                        return self.check_all_errors(kwargs['session_casher'], kkt_id, recid,
                                                                     check_type, user_name, error_other)
                                else:
                                    # пишем неверный статус чека
                                    error_other = "Статус чека %s, печать невозможна" % status
                                    return self.check_all_errors(kwargs['session_casher'], kkt_id, recid,
                                                                 check_type, user_name, error_other)
                            else:
                                return self.check_all_errors(kwargs['session_casher'], kkt_id, recid, check_type,
                                                             user_name)
                        elif env_p == "services" and check_type == 11:
                            # Проверка времени между ККТ и ПК перед системными операциями
                            f.put_RegisterNumber(17)
                            f.GetRegister()
                            kkt_date = f.get_Date()
                            kkt_time = f.get_Time()
                            kkt_datetime = datetime(int(kkt_date[2]), int(kkt_date[1]), int(kkt_date[0]),
                                                    int(kkt_time[0]), int(kkt_time[1]), int(kkt_time[2]))
                            now = datetime.now()

                            if now + timedelta(minutes=1) >= kkt_datetime >= now - timedelta(minutes=1):
                                pass
                            else:
                                error_other = 'Неверное время между ККТ и ПК'
                                return self.check_all_errors(kwargs['session_casher'], kkt_id, recid,
                                                             check_type, user_name, error_other)

                        if check_type == 1:
                            cr = "create_receipt_sell"
                        elif check_type == 2:
                            cr = "create_receipt_sell_return"
                        elif check_type == 7:
                            cr = "create_receipt_sell_correction"
                        elif check_type == 11:
                            cr = "open_session"
                        elif check_type == 12:
                            cr = "close_session"
                        elif check_type == 13:
                            cr = "report_x"
                        elif check_type == 14:
                            cr = "cancel_receipt"

                        # вызов функции с динамической подстановкой ее имени
                        if getattr(self.kkt, cr)(**kwargs):
                            task_result = 1
                            error_other = ""
                            # если чек оплаты успешно пробит, меняет записи в БД
                            if recid:
                                # получение последнего фискального документа, его номер, ФП
                                f.put_RegisterNumber(52)
                                f.GetRegister()
                                fd = int(f.get_DocNumber())
                                fpd = int(f.get_Value())

                                # меняем состояние чека и записываем фискальные данные
                                self.sql.update_status_receipt(recid, "3", fd, fpd)
                                return self.check_all_errors(kwargs['session_casher'], kkt_id, recid, check_type,
                                                             user_name, error_other, task_result)

                            # если сервисный чек открытия\закрытия смены
                            elif check_type == 11 or check_type == 12:
                                # получение номера смены
                                f.put_RegisterNumber(21)
                                f.GetRegister()
                                kkt_session = int(f.get_Session()) + 1
                                return self.check_all_errors(session_casher, kkt_id, kkt_session, check_type, user_name,
                                                             error_other, task_result)
                            else:
                                return self.check_all_errors(kwargs['session_casher'], kkt_id, recid, check_type,
                                                             user_name, error_other, task_result)
                        else:
                            # если чек открыт,то закрытие чека, после сбоя
                            self.kkt.cancel_receipt(f)
                            if cr:
                                self.sql.update_status_receipt(recid, "-1")
                            return self.check_all_errors(kwargs['session_casher'], kkt_id, recid, check_type, user_name)
                    # если нет подключеня к ккм
                    else:
                        error_other = "Нет связи с ККТ"
                        return self.check_all_errors(session_casher, kkt_id, recid, check_type, user_name, error_other)
                else:
                    return self.check_all_errors()
        elif env_p == str(""):
            return [b""]
        else:
            if env_p != "favicon.ic":
                return self.check_all_errors()
            else:
                return "favicon"

http = HttpOutput()
